const express = require("express");
const path = require("path");
const app = express();

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));


app.get('/login', (req, res) => {
    res.render('login', {
        title: 'Log In'
    })
})

app.get('/dashboard', (req, res) => {
    res.render('Dashboard', {
        title: 'Dashboard'
    })
})

app.get('/list-car', (req, res) => {
    res.render('list-car', {
        title: 'List Car'
    })
})


app.get('/add-new-car', (req, res) => {
    res.render('add-new-car', {
        title: 'Add New Car'
    })
})

app.listen(8000, () => {
    console.log("Server running on port 8000.");
});